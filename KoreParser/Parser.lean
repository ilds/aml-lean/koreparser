/-
  Copyright (c) 2023 Institute for Logic and Data Science (ILDS). All rights reserved.
  Released under MIT license as described in the file LICENSE.
  Authors: Horațiu Cheval

  Parser for Kore, using the Lean macro system,
  following closely the syntax from: `https://github.com/runtimeverification/haskell-backend/blob/master/docs/kore-syntax.md`.
-/

import Lean.Data.Parsec
import KoreParser.AST

set_option autoImplicit true
set_option relaxedAutoImplicit false

namespace Kore

open Lean Parsec

namespace Parsing

section Utils

def lexeme (p : Parsec α) : Parsec α :=
  p <* ws

def lexchar : Char → Parsec Char :=
  lexeme ∘ pchar

def lexstring : String → Parsec String :=
  lexeme ∘ pstring

def alpha : Parsec Char :=
  satisfy Char.isAlpha

def comma : Parsec Char :=
  lexchar ','

def whitespace : Parsec Unit :=
  satisfy Char.isWhitespace *> pure ()

def between (left : Parsec α) (right : Parsec β) (p : Parsec γ) : Parsec γ := do
  let _ ← left
  let x ← p
  let _ ← right
  return x

  def curly : Parsec α → Parsec α :=
    lexeme ∘ between (lexchar '{') (lexchar '}')

  def parens : Parsec α → Parsec α :=
    lexeme ∘ between (lexchar '(') (lexchar ')')

  def squares : Parsec α → Parsec α :=
    lexeme ∘ between (lexchar '[') (lexchar ']')

  def sepBy1 (p : Parsec α) (sep : Parsec β) : Parsec (Array α) := do
    let x : α ← p
    let xs : Array α ← many <| do let _ ← sep; p
    return ⟨x :: xs.toList⟩ -- bad smell: Array → List → Array

  def sepBy (p : Parsec α) (sep : Parsec β) : Parsec (Array α) :=
    sepBy1 p sep <|> return #[]

  def sepByExactly2 (p₁ : Parsec α) (p₂ : Parsec β) (sep : Parsec γ) : Parsec (α × β) := do
    let a ← p₁
    let _ ← sep
    let b ← p₂
    return (a, b)

end Utils

def strLit : Parsec String := do
  let arr ← lexeme <| between (pchar '"') (pchar '"') (many <| satisfy (. != '"'))
  return ⟨arr.toList⟩ -- is this inefficient

def kidentFirstChar : Parsec Char := alpha

def isKIdentOtherChar : Char → Bool :=
  fun c => c.isAlpha || c.isDigit || c == '-' || c == '\\'

def kidentOtherChar : Parsec Char := attempt do
  let c ← anyChar
  if isKIdentOtherChar c then return c else fail "expected alphanumeric, -, or backslash"

def isKeyword (str : String) :=
  [
      "module" , "endmodule"
    , "import"
    , "sort"   , "hooked-sort"
    , "symbol" , "hooked-symbol"
    , "axiom"  , "claim"
    , "alias"  , "where"
  ].contains str

def keyword (kw : String) : Parsec Unit := do
  if !isKeyword kw
    then fail s!"{kw} is not a keyword"
  let str ← lexeme <| pstring kw <* whitespace
  if str == kw then return () else fail s!"Keyword{kw} expected"

def kident : Parsec Identifier := lexeme <| attempt do
  let first ← alpha
  let other ← many kidentOtherChar
  let ident := toString first ++ ⟨other.toList⟩
  if !isKeyword ident then return ⟨ident⟩ else fail s! "{ident} is a keyword"

def symbolIdent : Parsec SymbolIdent := do
  -- let leadingBackslash? ← optional (pchar '\\') -- I think this should not be, maybe it is a mistake in the docs, because with it you cannot distinguish between app-pattern and ml-pattern
  return ⟨← kident⟩

def evarIdent : Parsec EVarIdent := do
  return ⟨← kident⟩

def svarIdent : Parsec SVarIdent  := do
  let _ ← pchar '@'
  return ⟨← kident⟩

def sortIdent : Parsec SortIdent := do
  return ⟨← kident⟩

def moduleName : Parsec ModuleName := do
  return ⟨← kident⟩

def sortVar : Parsec SortVar := do
  return ⟨← sortIdent⟩

def sortVars : Parsec (Array SortVar) :=
  curly <| sepBy sortVar comma

partial def ksort : Parsec KSort :=
    (attempt do
      let ident ← sortIdent
      let sorts ← curly <| sepBy ksort comma
      return .ctor ident sorts)
  <|>
    (do return .var (← sortVar))

def ksorts : Parsec (Array KSort) :=
  curly <| sepBy ksort comma

def evar : Parsec EVar := attempt do
  let ident ← evarIdent
  let _ ← lexchar ':'
  let sort ← ksort
  return ⟨ident, sort⟩

def svar : Parsec SVar := attempt do
  let ident ← svarIdent
  let _ ← lexchar ':'
  let sort ← ksort
  return ⟨ident, sort⟩

mutual

  partial def pattern : Parsec Pattern :=
        (do return .var        <| ← varPattern)
    <|> (do return .appPattern <| ← appPattern)
    <|> (do return .mlPattern  <| ← mlPattern)
    <|> (do return .string     <| ← strLit)

  partial def varPattern : Parsec VarPattern :=
    (do return .evar <| ← evar) <|> (do return .svar <| ← svar)

  partial def appPattern : Parsec AppPattern := do
    let symbol ← symbolIdent
    let sorts ← curly <| sepBy ksort comma
    let args ← parens <| sepBy pattern comma
    return ⟨symbol, sorts, args⟩

  partial def sortArg : Parsec KSort :=
    curly ksort

  partial def sortArgs2 : Parsec (KSort × KSort) :=
    curly <| sepByExactly2 ksort ksort comma

  partial def patternArg : Parsec Pattern :=
    parens pattern

  partial def patternArgs2 : Parsec (Pattern × Pattern) :=
    parens <| sepByExactly2 pattern pattern comma

  partial def top : Parsec MLPattern := attempt do
    return .top <| ← (lexstring "\\top") *> sortArg <* (parens ws)

  partial def bottom : Parsec MLPattern := attempt do
    return .bottom <| ← lexstring "\\bottom" *> sortArg <* (parens ws)

  partial def not : Parsec MLPattern := attempt do
    let sort ← lexstring "\\not" *> curly ksort
    let patt ← parens pattern
    return .not sort patt

  partial def and : Parsec MLPattern := attempt do
    let sort ← lexstring "\\and" *> curly ksort
    let patt ← parens <| sepByExactly2 pattern pattern comma
    return .and sort patt.1 patt.2

  partial def or : Parsec MLPattern := attempt do
    let sort ← lexstring "\\or" *> curly ksort
    let patt ← parens <| sepByExactly2 pattern pattern comma
    return .or sort patt.1 patt.2

  partial def implies : Parsec MLPattern := attempt do
    let sort ← lexstring "\\implies" *> curly ksort
    let patt ← parens <| sepByExactly2 pattern pattern comma
    return .implies sort patt.1 patt.2

  partial def iff : Parsec MLPattern := attempt do
    let sort ← lexstring "\\iff" *> curly ksort
    let patt ← parens <| sepByExactly2 pattern pattern comma
    return .iff sort patt.1 patt.2

  partial def exist : Parsec MLPattern := attempt do
    let sort ← lexstring "\\exists" *> curly ksort
    let (x, patt) ← parens <| sepByExactly2 evar pattern comma
    return .exist sort x patt

  partial def «forall» : Parsec MLPattern := attempt do
    let sort ← lexstring "\\exists" *> curly ksort
    let (x, patt) ← parens <| sepByExactly2 evar pattern comma
    return .exist sort x patt

  partial def mu : Parsec MLPattern := attempt do
    let _ ← lexstring "\\mu" *> curly ws
    let (X, patt) ← parens <| sepByExactly2 svar pattern comma
    return .mu X patt

  partial def nu : Parsec MLPattern := attempt do
    let _ ← lexstring "\\nu" *> curly ws
    let (X, patt) ← parens <| sepByExactly2 svar pattern comma
    return .nu X patt

  partial def ceil : Parsec MLPattern := attempt do
    let sorts ← lexstring "\\ceil" *> (curly <| sepByExactly2 ksort ksort comma)
    let patt ← parens pattern
    return .ceil sorts.1 sorts.2 patt

  partial def floor : Parsec MLPattern := attempt do
    let sorts ← lexstring "\\floor" *> (curly <| sepByExactly2 ksort ksort comma)
    let patt ← parens pattern
    return .floor sorts.1 sorts.2 patt

  partial def equals : Parsec MLPattern := attempt do
    let sorts ← lexstring "\\equals" *> (curly <| sepByExactly2 ksort ksort comma)
    let patts ← parens <| sepByExactly2 pattern pattern comma
    return .equals sorts.1 sorts.2 patts.1 patts.2

  partial def «in» : Parsec MLPattern := attempt do
    let sorts ← lexstring "\\in" *> (curly <| sepByExactly2 ksort ksort comma)
    let patts ← parens <| sepByExactly2 pattern pattern comma
    return .in sorts.1 sorts.2 patts.1 patts.2

  partial def next : Parsec MLPattern := attempt do
    return .next (← lexstring "\\next" *> curly ksort) (← parens pattern)

  partial def rewrites : Parsec MLPattern := attempt do
    let sort ← lexstring "\\rewrites" *> curly ksort
    let patts ← parens <| sepByExactly2 pattern pattern comma
    return .rewrites sort patts.1 patts.2

  partial def dv : Parsec MLPattern := attempt do
    let sort ← lexstring "\\dv" *> curly ksort
    let val ← parens strLit
    return .dv sort val

  partial def mlPattern : Parsec MLPattern :=
        top
    <|> bottom
    <|> not
    <|> and
    <|> or
    <|> implies
    <|> iff
    <|> exist
    <|> «forall»
    <|> mu
    <|> nu
    <|> ceil
    <|> floor
    <|> equals
    <|> «in»
    <|> next
    <|> rewrites
    -- <|> dv
end


def kattribute : Parsec KAttribute := do
  return ⟨← appPattern⟩

def kattributes : Parsec (Array KAttribute) :=
  squares <| sepBy kattribute comma

  #eval not.run "\\not{s}   (   \\top{  s    }  (     )  )"

mutual

  def sentenceImport : Parsec Import := do
    let moduleName ← keyword "import" *> moduleName
    let attrs ← kattributes
    return ⟨moduleName, attrs⟩

  def sentenceSort: Parsec SentenceSort := do
    let sortIdent ← keyword "sort" *> sortIdent
    let vars ← curly <| sepBy sortVar comma
    let attrs ← kattributes
    return ⟨sortIdent, vars, attrs⟩

  def sentenceHookedSort : Parsec HookedSort := do
    let sortIdent ← keyword "hooked-sort" *> sortIdent
    let vars ← curly <| sepBy sortVar comma
    let attrs ← kattributes
    return ⟨sortIdent, vars, attrs⟩

  def sentenceSymbol : Parsec SentenceSymbol := do
    let symbol ← keyword "symbol" *> symbol
    let sortArgs ← parens <| sepBy ksort comma
    let sort ← lexchar ':' *> ksort
    let attrs ← kattributes
    return ⟨symbol, sortArgs, sort, attrs⟩

  def sentenceHookedSymbol : Parsec HookedSymbol := do
    let symbol ← keyword "hooked-symbol" *> symbol
    let sortArgs ← parens <| sepBy ksort comma
    let sort ← lexchar ':' *> ksort
    let attrs ← kattributes
    return ⟨symbol, sortArgs, sort, attrs⟩

  def sentenceAlias : Parsec SentenceAlias := do
    let alias ← keyword "alias" *> alias
    let sortArgs ← parens <| sepBy ksort comma
    let sort ← lexchar ':' *> ksort
    let dfnHead ← keyword "where" *> appPattern
    let dfnBody ← lexstring ":=" *> pattern
    let attrs ← kattributes
    return ⟨alias, sortArgs, sort, dfnHead, dfnBody, attrs⟩

  def sentenceAxiom : Parsec Axiom := do
    let _ ← lexstring "axiom"
    let sortVars ← curly <| sepBy sortVar comma
    let patt ← pattern
    let attrs ← kattributes
    return ⟨sortVars, patt, attrs⟩

  def sentenceClaim : Parsec Claim := do
    let _ ← lexstring "claim"
    let sortVars ← curly <| sepBy sortVar comma
    let patt ← pattern
    let attrs ← kattributes
    return ⟨sortVars, patt, attrs⟩

  def symbolOrAlias : Parsec SymbolOrAlias := do
    return ⟨← symbolIdent, ← curly <| sepBy sortVar comma⟩

  def alias : Parsec Alias := do
    return ⟨← symbolOrAlias⟩

  def symbol : Parsec Symbol := do
    return ⟨← symbolOrAlias⟩

end

def sentence : Parsec Sentence :=
        (Sentence.«import»     <$> sentenceImport) -- which is nicer, `<| ←` or `<$>`? I guess it depends on how familiar one is with Haskell symbols
    <|> (do return .sort       <| ← sentenceSort)
    <|> (Sentence.hookedSort   <$> sentenceHookedSort)
    <|> (Sentence.symbol       <$> sentenceSymbol)
    <|> (Sentence.hookedSymbol <$> sentenceHookedSymbol)
    <|> (Sentence.alias        <$> sentenceAlias)
    <|> (Sentence.«axiom»      <$> sentenceAxiom)
    <|> (Sentence.claim        <$> sentenceClaim)

def module : Parsec Module := do
  let moduleName ← keyword "module" *> moduleName
  let sentences ← many sentence
  let _ ← keyword "endmodule"
  let attrs ← squares <| sepBy kattribute comma
  return ⟨moduleName, sentences, attrs⟩

def definition : Parsec Definition := do
  ws
  let attrs ← kattributes
  let modules ← sepBy module ws
  return ⟨attrs, modules⟩

def parseFile (fpath : System.FilePath) : IO (Except String Definition) := do
  let input ← IO.FS.readFile fpath
  return definition.run input

def input : String := " []

module BASIC-K
    sort SortK{} []
    sort SortKItem{} []
endmodule
[]
module KSEQ
    import BASIC-K []
    symbol kseq{}(SortKItem{}, SortK{}) : SortK{} [constructor{}(), functional{}(), injective{}()]
    symbol dotk{}() : SortK{} [constructor{}(), functional{}(), injective{}()]
    symbol append{}(SortK{}, SortK{}) : SortK{} [function{}(), functional{}()]
    axiom {R} \\implies{R}(
        \\and{R}(
            \\top{R}(),
            \\and{R}(
                \\in{SortK{}, R}(X0:SortK{}, dotk{}()),
            \\and{R}(
                \\in{SortK{}, R}(X1:SortK{}, TAIL:SortK{}),
                \\top{R}()
            ))
        ),
        \\equals{SortK{}, R}(
            append{}(X0:SortK{}, X1:SortK{}),
            \\and{SortK{}}(
                TAIL:SortK{},
                \\top{SortK{}}()
            )
        )
    ) []
    axiom {R} \\implies{R}(
        \\and{R}(
            \\top{R}(),
            \\and{R}(
                \\in{SortK{}, R}(X0:SortK{}, kseq{}(K:SortKItem{}, KS:SortK{})),
            \\and{R}(
                \\in{SortK{}, R}(X1:SortK{}, TAIL:SortK{}),
                \\top{R}()
            ))
        ),
        \\equals{SortK{}, R}(
            append{}(X0:SortK{}, X1:SortK{}),
            \\and{SortK{}}(
                kseq{}(K:SortKItem{}, append{}(KS:SortK{}, TAIL:SortK{})),
                \\top{SortK{}}()
            )
        )
    ) []
endmodule
[]
"

#eval definition.run input 
