/-
  Copyright (c) 2023 Institute for Logic and Data Science (ILDS). All rights reserved.
  Released under MIT license as described in the file LICENSE.
  Authors: Horațiu Cheval
-/

namespace Kore 

structure Identifier where 
  name : String
  deriving Repr 

instance : ToString Identifier where toString := Identifier.name
  
structure SymbolIdent where 
  ident : Identifier
  deriving Repr 

instance : ToString SymbolIdent where toString id := toString id.ident

structure EVarIdent where 
  ident : Identifier
  deriving Repr 

instance : ToString EVarIdent where toString id := toString id.ident

structure SVarIdent where 
  ident : Identifier
  deriving Repr 

instance : ToString SVarIdent where toString id := toString id.ident

structure SortIdent where 
  ident : Identifier
  deriving Repr 

instance : ToString SortIdent where toString id := toString id.ident  

structure ModuleName where 
  ident : Identifier 
  deriving Repr 

instance : ToString ModuleName where toString name := toString name.ident 

structure SortVar where 
  ident : SortIdent
  deriving Repr 

instance : ToString SortVar where toString v := toString v.ident 

inductive KSort where 
| var (var : SortVar)
| ctor (head : SortIdent) (args : Array KSort)
  deriving Repr 

partial instance : ToString KSort where -- we could make it non partial if we are not lazi, do we care?
  toString sort := 
    let rec aux sort' := match sort' with 
      | .var v => toString v 
      | .ctor head args => toString head ++ (args.map aux).foldl (.++.++" ") ""
    aux sort 

structure EVar where 
  var : EVarIdent
  sort : KSort
  deriving Repr 

instance : ToString EVar where 
  toString := fun ⟨var, sort⟩ => s! "{var} : {sort}"

structure SVar where 
  var : SVarIdent
  sort : KSort
  deriving Repr 

instance : ToString SVar where 
  toString := fun ⟨var, sort⟩ => s! "{var} : {sort}"

inductive VarPattern where 
| evar : EVar → VarPattern 
| svar : SVar → VarPattern 
  deriving Repr 

instance : ToString VarPattern where toString φ := match φ with | .evar v | .svar v => toString v

structure StrLit where 
  strLit : String 
  deriving Repr 

instance : ToString StrLit where toString := StrLit.strLit 

mutual 
  inductive Pattern where 
  | var : VarPattern → Pattern 
  | mlPattern : MLPattern → Pattern 
  | appPattern : AppPattern → Pattern 
  | string : String → Pattern 
    deriving Repr 

  inductive AppPattern where 
  | mk (head : SymbolIdent) (sorts : Array KSort) (args : Array Pattern) : AppPattern 
    deriving Repr 

  inductive MultiOr where 
  | mk : KSort → Array Pattern → MultiOr 
    deriving Repr 

  inductive MLPattern where 
  | top (sort : KSort)
  | bottom (sort : KSort)
  | not (sort : KSort) (patt : Pattern)
  | and (sort : KSort) (patt₁ patt₂ : Pattern)
  | or (sort : KSort) (patt₁ patt₂ : Pattern)
  | implies (sort : KSort) (patt₁ patt₂ : Pattern)
  | iff (sort : KSort) (patt₁ patt₂ : Pattern)
  | exist (sort : KSort) (var : EVar) (patt : Pattern)
  | forall (sort : KSort) (var : EVar) (patt : Pattern)
  | mu (var : SVar) (patt : Pattern)
  | nu (var : SVar) (patt : Pattern)
  | ceil (source : KSort) (target : KSort) (patt : Pattern)
  | floor (source : KSort) (target : KSort) (patt : Pattern)
  | equals : KSort → KSort → Pattern → Pattern → MLPattern 
  | «in» : KSort → KSort → Pattern → Pattern → MLPattern 
  | next : KSort → Pattern → MLPattern 
  | rewrites : KSort → Pattern → Pattern → MLPattern  
  | dv : KSort → String → MLPattern 
  | leftAssoc : AppPattern → MLPattern  
  | leftAssocMultiOr : MultiOr → MLPattern  
  | rightAssoc : AppPattern → MLPattern  
  | rightAssocMultiOr : MultiOr → MLPattern  
    deriving Repr 
end 

mutual 

  protected partial def Pattern.toString : Pattern → String 
    | .var φ => toString φ 
    | .mlPattern φ => φ.toString 
    | .appPattern φ => φ.toString 
    | .string str => str 

  protected partial def AppPattern.toString : AppPattern → String 
    | .mk head sorts args => 
      (toString head) ++( sorts.zip args |>.map (fun (sort, arg) => s! "({sort}, {arg.toString})") |>.foldl (.++.++" ") "")

  protected partial def MultiOr.toString : MultiOr → String := fun _ => "placeholder; what is a multi-or?"

  protected partial def MLPattern.toString : MLPattern → String 
    | .top sort => s! "⊤<{sort}>"
    | .bottom sort => s! "⊥<{sort}>"
    | .not sort φ => s! "¬<{sort}>{φ.toString}" 
    | .and sort φ₁ φ₂ => s! "{φ₁.toString} ∧<{sort}> {φ₂.toString}"
    | .or sort φ₁ φ₂ => s! "{φ₁.toString} ∨<{sort}> {φ₂.toString}"
    | .implies sort φ₁ φ₂ => s! "{φ₁.toString} →<{sort}> {φ₂.toString}"
    | .iff sort φ₁ φ₂ => s! "{φ₁.toString} ↔<{sort}> {φ₂.toString}"
    | .exist sort x φ => s! "∃<{sort}> {x} {φ.toString}"
    | .forall sort x φ => s! "∀<{sort}> {x} {φ.toString}"
    | .mu X φ => s! "μ {X} {φ.toString}"
    | .nu X φ => s! "ν {X} {φ.toString}"
    | .ceil src tgt φ => s! "⌈{φ.toString}⌉<{src}, {tgt}>"
    | .floor src tgt φ => s! "⌊{φ.toString}⌋<{src}, {tgt}>"
    | .equals s₁ s₂ φ₁ φ₂ => s! "{φ₁.toString} <{s₁}>=<{s₂}> {φ₂.toString}" 
    | .«in» s₁ s₂ φ₁ φ₂ => s! "{φ₁.toString} <{s₁}>⊆<{s₂}> {φ₂.toString}"
    | .next s φ => s! "next<{s}> {φ.toString}"
    | .rewrites s φ₁ φ₂ => s! "{φ₁.toString} ⟹<{s}> {φ₂.toString}"
    | .dv s str => s! "dv<{s}> {str}"
    | .leftAssoc φ => s! "left-assoc {φ.toString}"
    | .leftAssocMultiOr m => s! "left-assoc {m.toString}"
    | .rightAssoc φ => s! "right-assoc {φ.toString}"
    | .rightAssocMultiOr m => s! "right-assoc {m.toString}"
end 

instance : ToString Pattern where toString := Pattern.toString 
instance : ToString AppPattern where toString := AppPattern.toString 
instance : ToString MultiOr where toString := MultiOr.toString 
instance : ToString MLPattern where toString := MLPattern.toString 

mutual 

  inductive Alias where 
  | mk : SymbolOrAlias → Alias 
    deriving Repr 

  inductive Symbol where 
  | mk : SymbolOrAlias → Symbol 
    deriving Repr 

  inductive SymbolOrAlias where 
  | mk : SymbolIdent → Array SortVar → SymbolOrAlias   
    deriving Repr 

end 

structure KAttribute where 
  pattern : AppPattern 
  deriving Repr 

structure Import where 
  module : ModuleName 
  attributes : Array KAttribute 
  deriving Repr 

structure SentenceSort where 
  sort : SortIdent 
  sortVars : Array SortVar 
  attributes : Array KAttribute 
  deriving Repr 

structure HookedSort where 
  sort : SortIdent 
  sortVars : Array SortVar 
  attributes : Array KAttribute 
  deriving Repr 

structure SentenceSymbol where 
  symbol : Symbol 
  sorts : Array KSort 
  sort : KSort 
  attributes : Array KAttribute 
  deriving Repr 

structure HookedSymbol where 
  symbol : Symbol 
  sorts : Array KSort 
  sort : KSort 
  attributes : Array KAttribute 
  deriving Repr 

structure SentenceAlias where 
  alias : Alias 
  sorts : Array KSort 
  sort : KSort 
  app : AppPattern 
  body : Pattern 
  attributes : Array KAttribute 
  deriving Repr 

structure Axiom where 
  sortVars : Array SortVar 
  pattern : Pattern 
  attributes : Array KAttribute 
  deriving Repr 

structure Claim where 
  sortVars : Array SortVar 
  pattern : Pattern 
  attributes : Array KAttribute 
  deriving Repr 

inductive Sentence where 
| «import» : Import → Sentence 
| sort : SentenceSort → Sentence 
| hookedSort : HookedSort → Sentence 
| symbol : SentenceSymbol → Sentence 
| hookedSymbol : HookedSymbol → Sentence 
| alias : SentenceAlias → Sentence 
| «axiom» : Axiom → Sentence 
| claim : Claim → Sentence 
  deriving Repr    

structure Module where 
  name : ModuleName 
  sentences : Array Sentence 
  attributes : Array KAttribute 
  deriving Repr 

structure Definition where 
  attributes : Array KAttribute 
  modules : Array Module 
  deriving Repr 
