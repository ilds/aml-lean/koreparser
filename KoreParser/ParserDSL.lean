/-
Copyright (c) 2023 Institute for Logic and Data Science (ILDS). All rights reserved.
Released under MIT license as described in the file LICENSE.
Authors: Horațiu Cheval

Parser for Kore, using the Lean macro system,
following closely the syntax from: `https://github.com/runtimeverification/haskell-backend/blob/master/docs/kore-syntax.md`.
-/


import Lean
import Lean.Parser
import KoreParser.AST

set_option autoImplicit false

namespace Kore



namespace KoreSyntax

  -- I think this works, I'm not sure
  open Lean Parser in
  def kidentNoAntiquot : Parser where
    fn ctx state :=
      let initStackSize := state.stackSize
      let input := ctx.input
      let startPos := state.pos
      if input.atEnd startPos then
        state.mkEOIError ["unexpected end of input"]
      else
        let state := satisfyFn (fun c => c.isAlpha || c.isDigit) "" ctx state
        let state := takeWhileFn (fun c => c.isAlpha || c.isDigit || c == '-') ctx state
        let state := mkNodeToken `kidentAux startPos ctx state
        if state.hasError then state.mkErrorAt "kidentNoAntiquot expected" startPos initStackSize else state

  @[combinator_formatter kidentNoAntiquot]
  def kidentNoAntiquot.formatter : Lean.PrettyPrinter.Formatter := pure ()
  @[combinator_parenthesizer kidentNoAntiquot]
  def kidentNoAntiquot.parenthesizer : Lean.PrettyPrinter.Parenthesizer := pure ()

  open Lean Parser in
  @[run_builtin_parser_attribute_hooks] def kidentAux : Parser :=
    withAntiquot (mkAntiquot "kidentAux" `kidentAux) kidentNoAntiquot

  -- @[combinator_formatter kidentParser]
  -- def kidentParser.formatter : Lean.PrettyPrinter.Formatter := pure ()
  -- @[combinator_parenthesizer kidentParser]
  -- def kidentParser.parenthesizer : Lean.PrettyPrinter.Parenthesizer := pure ()

  declare_syntax_cat kident
  scoped syntax kidentAux : kident

  declare_syntax_cat sort_ident
  scoped syntax kident : sort_ident

  declare_syntax_cat evar_ident
  scoped syntax kident : evar_ident

  declare_syntax_cat svar_ident
  scoped syntax kident : svar_ident

  declare_syntax_cat symbol_ident
  scoped syntax kident : symbol_ident

  declare_syntax_cat module_name
  scoped syntax kident : module_name

  declare_syntax_cat sort_var
  scoped syntax sort_ident : sort_var

  declare_syntax_cat ksort
  scoped syntax sort_var : ksort
  scoped syntax sort_ident "{" ksort,* "}" : ksort

  declare_syntax_cat evar
  scoped syntax evar_ident ":" ksort : evar

  declare_syntax_cat svar
  scoped syntax "@" svar_ident ":" ksort : svar

  /- # Patterns #-/

  declare_syntax_cat pattern
  declare_syntax_cat var_pattern
  declare_syntax_cat ml_pattern
  declare_syntax_cat app_pattern
  declare_syntax_cat multi_or

  scoped syntax ml_pattern : pattern
  scoped syntax app_pattern : pattern
  scoped syntax var_pattern : pattern

  scoped syntax evar : var_pattern
  scoped syntax svar : var_pattern

  scoped syntax symbol_ident "{" ksort,* "}" "(" pattern,* ")" : app_pattern

  scoped syntax "\\or" "{" ksort "}" "(" pattern,* ")" : multi_or

  --to remember: "{}" is different "{" "}" in a parser
  scoped syntax "\\top" "{" ksort "}" "(" ")" : ml_pattern
  scoped syntax "\\bottom" "{" ksort "}" "(" ")" : ml_pattern
  scoped syntax "\\not" "{" ksort "}" "(" pattern ")" : ml_pattern
  scoped syntax "\\and" "{" ksort "}" "(" pattern "," pattern ")" : ml_pattern
  scoped syntax "\\or" "{" ksort "}" "(" pattern "," pattern ")" : ml_pattern
  scoped syntax "\\implies" "{" ksort "}" "(" pattern "," pattern ")" : ml_pattern
  scoped syntax "\\iff" "{" ksort "}" "(" pattern "," pattern ")" : ml_pattern
  scoped syntax "\\exists" "{" ksort "}" "(" evar "," pattern ")" : ml_pattern
  scoped syntax "\\forall" "{" ksort "}" "(" evar "," pattern ")" : ml_pattern
  scoped syntax "\\mu" "{" "}" "(" svar "," pattern ")" : ml_pattern
  scoped syntax "\\nu" "{" "}" "(" svar "," pattern ")" : ml_pattern
  scoped syntax "\\ceil" "{" ksort "," ksort "}" "(" pattern ")" : ml_pattern
  scoped syntax "\\floor" "{" ksort "," ksort "}" "(" pattern ")" : ml_pattern
  scoped syntax "\\equals" "{" ksort "," ksort "}" "(" pattern "," pattern ")" : ml_pattern
  scoped syntax "\\in" "{" ksort "," ksort "}" "(" pattern "," pattern ")" : ml_pattern
  scoped syntax "\\next" "{" ksort "}" "(" pattern ")" : ml_pattern
  scoped syntax "\\rewrites" "{" ksort "}" "(" pattern "," pattern ")" : ml_pattern
  scoped syntax "\\dv" "{" ksort "}" "(" str ")" : ml_pattern
  scoped syntax "\\left-assoc" "{" "}" "(" app_pattern ")" : ml_pattern
  scoped syntax "\\left-assoc" "{" "}" "(" multi_or ")" : ml_pattern
  scoped syntax "\\right-assoc" "{" "}" "(" app_pattern ")" : ml_pattern
  scoped syntax "\\right-assoc" "{" "}" "(" multi_or ")" : ml_pattern




  declare_syntax_cat kattribute
  scoped syntax app_pattern : kattribute



  declare_syntax_cat sentence
  declare_syntax_cat sentence_import
  declare_syntax_cat sentence_sort
  declare_syntax_cat hooked_sort
  declare_syntax_cat sentence_symbol
  declare_syntax_cat hooked_symbol
  declare_syntax_cat sentence_alias
  declare_syntax_cat sentence_axiom
  declare_syntax_cat sentence_claim
  declare_syntax_cat alias
  declare_syntax_cat symbol
  declare_syntax_cat symbol_or_alias

  scoped syntax sentence_import : sentence
  scoped syntax sentence_sort : sentence
  scoped syntax hooked_sort : sentence
  scoped syntax sentence_symbol : sentence
  scoped syntax hooked_symbol : sentence
  scoped syntax sentence_alias : sentence
  scoped syntax sentence_axiom : sentence
  scoped syntax sentence_claim : sentence

  scoped syntax "import" module_name "[" kattribute,* "]" : sentence_import

  scoped syntax "sort" sort_ident "{" sort_var,* "}" "[" kattribute,* "]" : sentence_sort

  scoped syntax "hooked-sort" sort_ident "{" sort_var,* "}" "[" kattribute,* "]" : hooked_sort

  scoped syntax "symbol" «symbol» "(" ksort,* ")" ":" ksort "[" kattribute,* "]" : sentence_symbol

  scoped syntax "hooked-symbol" «symbol» "(" ksort,* ")" ":" ksort "[" kattribute,* "]" : hooked_symbol

  scoped syntax "alias" alias "(" ksort,* ")" ":" ksort "where" app_pattern ":=" pattern "[" kattribute,* "]" : sentence_alias

  scoped syntax "axiom" "{" sort_var,* "}" pattern "[" kattribute,* "]" : sentence_axiom

  scoped syntax "claim" "{" sort_var,* "}" pattern "[" kattribute,* "]" : sentence_claim

  scoped syntax symbol_or_alias : «alias»
  scoped syntax symbol_or_alias : «symbol»
  scoped syntax symbol_ident "{" sort_var,* "}" : symbol_or_alias


  declare_syntax_cat «definition»
  declare_syntax_cat kmodule

  scoped syntax "[" kattribute,* "]" kmodule,* : definition --how to separate by whitespace?
  scoped syntax "module" module_name sentence* "endmodule" "[" kattribute,* "]" : kmodule

end KoreSyntax


section Elaborators

  open Lean Elab Meta
  open KoreSyntax

  def elabKIdent : TSyntax `kident → Except String Identifier
    | stx@`(kident| $id:kidentAux) =>
      dbg_trace stx
      -- hack
      let raw := toString stx.raw[0][0]
      return ⟨toString <| Substring.mk raw (raw.next 0) (raw.prev raw.endPos)⟩
    | _ => throw "unsupported syntax"

  def elabSortIdent : TSyntax `sort_ident → Except String SortIdent
    | `(sort_ident| $kid:kident) => return ⟨← elabKIdent kid⟩
    | _ => throw "unsupported syntax"

  def elabEVarIdent : TSyntax `evar_ident → Except String EVarIdent
    | `(evar_ident| $id:kident) => return ⟨← elabKIdent id⟩
    | _ => throw "unsupported syntax"

  def elabSVarIdent : TSyntax `svar_ident → Except String SVarIdent
    | `(evar_ident| $id:kident) => return ⟨← elabKIdent id⟩
    | _ => throw "unsupported syntax"

  def elabSymbolIdent : TSyntax `symbol_ident → Except String SymbolIdent
    | `(symbol_ident| $id:kident) => return ⟨← elabKIdent id⟩
    | _ => throw "unsupported syntax"

  def elabModuleName : TSyntax `module_name → Except String ModuleName
    | `(module_name| $id:kident) => return ⟨← elabKIdent id⟩
    | _ => throw "unsupported syntax"

  def elabSortVar : TSyntax `sort_var → Except String SortVar
    | `(sort_var| $id:sort_ident) => return ⟨← elabSortIdent id⟩
    | _ => throw "unsupported syntax"

  def elabSortVars : Syntax.TSepArray `sort_var "," → Except String (Array SortVar) :=
    fun stx => stx.getElems.mapM elabSortVar

  partial def elabKSort : TSyntax `ksort → Except String KSort
    | `(ksort| $v:sort_var) =>
      return KSort.var (← elabSortVar v)
    | `(ksort| $id:sort_ident{$sorts,*}) =>
      return .ctor (← elabSortIdent id) (← sorts.getElems.mapM elabKSort)
    | _ => throw "unsupported syntax"

  def elabKSorts : Syntax.TSepArray `ksort "," → Except String (Array KSort) :=
    fun stx => stx.getElems.mapM elabKSort

  def elabEVar : TSyntax `evar → Except String EVar
    | `(evar| $id:evar_ident : $s:ksort) =>
      return ⟨← elabEVarIdent id, ← elabKSort s⟩
    | _ => throw "unsupported syntax"

  def elabSVar : TSyntax `svar → Except String SVar
    | `(svar| @$id:svar_ident : $s:ksort) =>
      return ⟨← elabSVarIdent id, ← elabKSort s⟩
    | _ => throw "unsupported syntax"

  def elabVarPattern : TSyntax `var_pattern → Except String VarPattern
    | `(var_pattern| $x:evar) =>
      return .evar (← elabEVar x)
    | `(var_pattern| $X:svar) =>
      return .svar (← elabSVar X)
    | _ => throw "unsupported syntax"

  mutual
    partial def elabAppPattern : TSyntax `app_pattern → Except String AppPattern
      | `(app_pattern| $σ:symbol_ident{$sorts:ksort,*}($args:pattern,*)) =>
        return .mk
          (head := ← elabSymbolIdent σ)
          (sorts := ← sorts.getElems.mapM elabKSort)
          (args := ← args.getElems.mapM elabPattern)
      | _ => throw "unsupported syntax"

    partial def elabMultiOr : TSyntax `multi_or → Except String MultiOr
      | `(multi_or| \or{$s:ksort}($args:pattern,*)) =>
        return ⟨← elabKSort s, ← args.getElems.mapM elabPattern⟩
      | _ => throw "unsupported syntax"

    partial def elabMLPattern : TSyntax `ml_pattern → Except String MLPattern
      | `(ml_pattern| \top{$s:ksort}()) =>
        return .top (← elabKSort s)
      | `(ml_pattern| \bottom{$s:ksort}()) =>
        return .bottom (← elabKSort s)
      | `(ml_pattern| \not{$s:ksort}($φ:pattern)) =>
        return .not (← elabKSort s) (← elabPattern φ)
      | `(ml_pattern| \and{$s:ksort}($φ:pattern, $ψ:pattern)) =>
        return .and (← elabKSort s) (← elabPattern φ) (← elabPattern ψ)
      | `(ml_pattern| \or{$s:ksort}($φ:pattern, $ψ:pattern)) =>
        return .or (← elabKSort s) (← elabPattern φ) (← elabPattern ψ)
      | `(ml_pattern| \implies{$s:ksort}($φ:pattern, $ψ:pattern)) =>
        return .implies (← elabKSort s) (← elabPattern φ) (← elabPattern ψ)
      | `(ml_pattern| \iff{$s:ksort}($φ:pattern, $ψ:pattern)) =>
        return .iff (← elabKSort s) (← elabPattern φ) (← elabPattern ψ)
      | `(ml_pattern| \exists{$s:ksort}($x:evar, $φ:pattern)) =>
        return .exist (← elabKSort s) (← elabEVar x) (← elabPattern φ)
      | `(ml_pattern| \forall{$s:ksort}($x:evar, $φ:pattern)) =>
        return .forall (← elabKSort s) (← elabEVar x) (← elabPattern φ)
      | `(ml_pattern| \mu{}($X:svar, $φ:pattern)) =>
        return .mu (← elabSVar X) (← elabPattern φ)
      | `(ml_pattern| \nu{}($X:svar, $φ:pattern)) =>
        return .mu (← elabSVar X) (← elabPattern φ)
      | `(ml_pattern| \ceil{$s₁:ksort, $s₂:ksort}($φ:pattern)) =>
        return .ceil (← elabKSort s₁) (← elabKSort s₂) (← elabPattern φ)
      | `(ml_pattern| \floor{$s₁:ksort, $s₂:ksort}($φ:pattern)) =>
        return .floor (← elabKSort s₁) (← elabKSort s₂) (← elabPattern φ)
      | `(ml_pattern| \equals{$s₁:ksort, $s₂:ksort}($φ₁:pattern, $φ₂:pattern)) =>
        return .equals (← elabKSort s₁) (← elabKSort s₂) (← elabPattern φ₁) (← elabPattern φ₂)
      | `(ml_pattern| \in{$s₁:ksort, $s₂:ksort}($φ₁:pattern, $φ₂:pattern)) =>
        return .«in» (← elabKSort s₁) (← elabKSort s₂) (← elabPattern φ₁) (← elabPattern φ₂)
      | `(ml_pattern| \next{$s:ksort}($φ:pattern)) =>
        return .next (← elabKSort s) (← elabPattern φ)
      | `(ml_pattern| \rewrites{$s:ksort}($φ₁:pattern, $φ₂:pattern)) =>
        return .rewrites (← elabKSort s) (← elabPattern φ₁) (← elabPattern φ₂)
      | `(ml_pattern| \dv{$s:ksort}($string:str)) =>
        return .dv (← elabKSort s) string.getString
      | `(ml_pattern| \left-assoc{}($φ:app_pattern)) =>
        return .leftAssoc (← elabAppPattern φ)
      | `(ml_pattern| \left-assoc{}($φ:multi_or)) =>
        return .leftAssocMultiOr (← elabMultiOr φ)
      | `(ml_pattern| \right-assoc{}($φ:app_pattern)) =>
        return .rightAssoc (← elabAppPattern φ)
      | `(ml_pattern| \right-assoc{}($φ:multi_or)) =>
        return .rightAssocMultiOr (← elabMultiOr φ)
      | _ => throw "unsupported syntax"

    partial def elabPattern : TSyntax `pattern → Except String Pattern
      | `(pattern| $φ:ml_pattern) => return .mlPattern (← elabMLPattern φ)
      | `(pattern| $φ:app_pattern) => return .appPattern (← elabAppPattern φ)
      | `(pattern| $φ:var_pattern) => return .var (← elabVarPattern φ)
      | _ => throw "unsupported syntax"
  end



  def elabKAttribute : TSyntax `kattribute → Except String KAttribute
    | `(kattribute| $φ:app_pattern) =>
      return ⟨← elabAppPattern φ⟩
    | _ => throw "unsupported syntax"


  def elabKAttributes : Syntax.TSepArray `kattribute "," → Except String (Array KAttribute) :=
    fun stx => stx.getElems.mapM elabKAttribute

  mutual

    def elabSentenceImport : TSyntax `sentence_import → Except String Import
      | `(sentence_import| import $name:module_name [$attrs:kattribute,*]) =>
        return ⟨← elabModuleName name, ← elabKAttributes attrs⟩
      | _ => throw "unsupported syntax"

    def elabSentenceSort : TSyntax `sentence_sort → Except String SentenceSort
      | `(sentence_sort| sort $id:sort_ident {$vars:sort_var,*} [$attrs:kattribute,*]) =>
        return ⟨← elabSortIdent id, ← elabSortVars vars, ← elabKAttributes attrs⟩
      | _ => throw "unsupported syntax"

    def elabHookedSort : TSyntax `hooked_sort → Except String HookedSort
      | `(hooked_sort| hooked-sort $id:sort_ident {$vars:sort_var,*} [$attrs:kattribute,*]) =>
        return ⟨← elabSortIdent id, ← elabSortVars vars, ← elabKAttributes attrs⟩
      | _ => throw "unsupported syntax"

    def elabSentenceSymbol : TSyntax `sentence_symbol → Except String SentenceSymbol
      | `(sentence_symbol| symbol $σ:symbol ($sorts:ksort,*) : $«sort»:ksort [$attrs:kattribute,*]) =>
        return ⟨← elabKSymbol σ, ← elabKSorts sorts, ← elabKSort «sort», ← elabKAttributes attrs⟩
      | _ => throw "unsupported syntax"

    def elabHookedSymbol : TSyntax `hooked_symbol → Except String HookedSymbol
      | `(hooked_symbol| hooked-symbol $σ:symbol ($sorts:ksort,*) : $«sort»:ksort [$attrs:kattribute,*]) =>
        return ⟨← elabKSymbol σ, ← elabKSorts sorts, ← elabKSort «sort», ← elabKAttributes attrs⟩
      | _ => throw "unsupported syntax"

    def elabSentenceAlias : TSyntax `sentence_alias → Except String SentenceAlias
      | `(sentence_alias| alias $a:alias ($sorts:ksort,*) : $s:ksort where $φ:app_pattern := $ψ:pattern [$attrs:kattribute,*]) =>
        return ⟨← elabAlias a, ← elabKSorts sorts, ← elabKSort s, ← elabAppPattern φ, ← elabPattern ψ, ← elabKAttributes attrs⟩
      | _ => throw "unsupported syntax"

    def elabSentenceAxiom : TSyntax `sentence_axiom → Except String Axiom
      | `(sentence_axiom| axiom {$vars:sort_var,*} $φ:pattern [$attrs:kattribute,*]) =>
        return ⟨← elabSortVars vars, ← elabPattern φ, ← elabKAttributes attrs⟩
      | _ => throw "unsupported syntax"

    def elabSentenceClaim : TSyntax `sentence_claim → Except String Claim
      | `(sentence_claim| claim {$vars:sort_var,*} $φ:pattern [$attrs:kattribute,*]) =>
        return ⟨← elabSortVars vars, ← elabPattern φ, ← elabKAttributes attrs⟩
      | _ => throw "unsupported syntax"

    def elabAlias : TSyntax `alias → Except String Alias
      | `(alias| $sa:symbol_or_alias) =>
        return ⟨← elabSymbolOrAlias sa⟩
      | _ => throw "unsupported syntax"

    def elabKSymbol : TSyntax `symbol → Except String Symbol
      | `(symbol| $sa:symbol_or_alias) =>
        return ⟨← elabSymbolOrAlias sa⟩
      | _ => throw "unsupported syntax"

    def elabSymbolOrAlias : TSyntax `symbol_or_alias → Except String SymbolOrAlias
      | `(symbol_or_alias| $id:symbol_ident {$vars:sort_var,*}) =>
        return ⟨← elabSymbolIdent id, ← elabSortVars vars⟩
      | _ => throw "unsupported syntax"

    def elabSentence : TSyntax `sentence → Except String Sentence
      | `(sentence| $s:sentence_import) =>
        return .«import» <| ← elabSentenceImport s
      | `(sentence| $s:sentence_sort) =>
        return .sort <| ← elabSentenceSort s
      | `(sentence| $s:hooked_sort) =>
        return .hookedSort <| ← elabHookedSort s
      | `(sentence| $s:sentence_symbol) =>
        return .symbol <| ← elabSentenceSymbol s
      | `(sentence| $s:hooked_symbol) =>
        return .hookedSymbol <| ← elabHookedSymbol s
      | `(sentence| $s:sentence_alias) =>
        return .alias <| ← elabSentenceAlias s
      | `(sentence| $s:sentence_axiom) =>
        return .«axiom» <| ← elabSentenceAxiom s
      | `(sentence| $s:sentence_claim) =>
        return .claim <| ← elabSentenceClaim s
      | _ => throw "unsupported syntax"


  end


#check TSyntaxArray
  def elabModule : TSyntax `kmodule → Except String Module
    | `(kmodule| module $name:module_name $sents:sentence* endmodule [$attrs:kattribute,*]) =>
      return ⟨← elabModuleName name, ← sents.mapM elabSentence, ← elabKAttributes attrs⟩
    | _ => throw "unsupported syntax"

  def elabDefinition : TSyntax `definition → Except String Definition
    | `(definition| [$attrs:kattribute,*] $mods:kmodule,*) =>
      return ⟨← elabKAttributes attrs, ← mods.getElems.mapM elabModule⟩
    | _ => throw "unsupported syntax"


  class ElabKore (name : Name) (α : outParam Type) where
    elabKore : TSyntax name → Except String α


  #check Parser.runParserCategory

end Elaborators

open Lean


open scoped KoreSyntax in set_option hygiene false in
#eval show MetaM Unit from do

  let stx : TSyntax `kmodule ← `(kmodule|
    module KSEQ
    import BASIC-K []
    symbol kseq{}(SortKItem{}, SortK{}) : SortK{} [constructor{}(), functional{}(), injective{}()]
    symbol dotk{}() : SortK{} [constructor{}(), functional{}(), injective{}()]
    symbol append{}(SortK{}, SortK{}) : SortK{} [function{}(), functional{}()]
    axiom {R} \implies{R}(
        \and{R}(
            \top{R}(),
            \and{R}(
                \in{SortK{}, R}(X0:SortK{}, dotk{}()),
            \and{R}(
                \in{SortK{}, R}(X1:SortK{}, TAIL:SortK{}),
                \top{R}()
            ))
        ),
        \equals{SortK{}, R}(
            append{}(X0:SortK{}, X1:SortK{}),
            \and{SortK{}}(
                TAIL:SortK{},
                \top{SortK{}}()
            )
        )
    ) []
    axiom {R} \implies{R}(
        \and{R}(
            \top{R}(),
            \and{R}(
                \in{SortK{}, R}(X0:SortK{}, kseq{}(K:SortKItem{}, KS:SortK{})),
            \and{R}(
                \in{SortK{}, R}(X1:SortK{}, TAIL:SortK{}),
                \top{R}()
            ))
        ),
        \equals{SortK{}, R}(
            append{}(X0:SortK{}, X1:SortK{}),
            \and{SortK{}}(
                kseq{}(K:SortKItem{}, append{}(KS:SortK{}, TAIL:SortK{})),
                \top{SortK{}}()
            )
        )
    ) []
endmodule
[]
)
  let s := elabModule stx
  dbg_trace repr s

def parseKorePatternString (env : Lean.Environment) (source : String) : Except String Pattern := do
  let stx ← Lean.Parser.runParserCategory env `pattern source
  elabPattern ⟨stx⟩

def parseFile (fpath : System.FilePath) : ExceptT String CoreM Definition := do
  let input ← IO.FS.readFile fpath
  let stx : TSyntax `definition := .mk <| ← Lean.Parser.runParserCategory (← getEnv) `definition input
  elabDefinition stx
